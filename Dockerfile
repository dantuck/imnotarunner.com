FROM node:14-alpine
# RUN npm install -g @11ty/eleventy

WORKDIR /usr/src
COPY . /usr/src/

RUN npm ci
RUN npm run build

#this lines are not in use for multistage build
#EXPOSE 8081
#CMD [ "eleventy", "--serve", "--port=8081"]

FROM nginx:alpine

COPY --from=0 /usr/src/dist /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf
