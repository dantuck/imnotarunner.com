module.exports = {
  purge: process.env.NODE_ENV === 'production' ? {
    content: [
      "src/**/*.njk",
      "src/**/*.md"
    ],
    options: {
      whitelist: [],
    },
  } : {},
  theme: {},
  variants: {},
  plugins: [],
};